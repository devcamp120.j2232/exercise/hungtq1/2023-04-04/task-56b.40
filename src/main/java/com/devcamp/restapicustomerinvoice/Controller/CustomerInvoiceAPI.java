package com.devcamp.restapicustomerinvoice.Controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapicustomerinvoice.Controller.models.Customer;
import com.devcamp.restapicustomerinvoice.Controller.models.Invoice;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CustomerInvoiceAPI {
  @GetMapping("/invoices")
  public ArrayList<Invoice> getInvoiceList() {
    Customer customer1 = new Customer(1, "Teo", 20);
    Customer customer2 = new Customer(2, "Binh", 30);
    Customer customer3 = new Customer(3, "Manh", 25);
    System.out.println(customer1.toString());
    System.out.println(customer2.toString());
    System.out.println(customer3.toString());

    Invoice invoice1 = new Invoice(1, customer1, 100000);
    Invoice invoice2 = new Invoice(2, customer2, 170000);
    Invoice invoice3 = new Invoice(3, customer3, 150000);
    System.out.println(invoice1.toString());
    System.out.println(invoice2.toString());
    System.out.println(invoice3.toString());

    ArrayList<Invoice> invoices = new ArrayList<>();
    invoices.add(invoice1);
    invoices.add(invoice2);
    invoices.add(invoice3);

    return invoices;
  }
}
